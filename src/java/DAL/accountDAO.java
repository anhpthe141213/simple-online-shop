/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;

/**
 *
 * @author msi
 */
public class accountDAO extends BaseDAO<Account> {

    @Override
    public ArrayList<Account> getAll() {
        ArrayList<Account> acc = new ArrayList<>();
        try {
            String sql = "SELECT [username]\n"
                    + "      ,[password]\n"
                    + "  FROM [accounts]";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Account a = new Account();
                a.setUser(rs.getString("username"));
                a.setPass(rs.getString("password"));
                acc.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return acc;
    }

    String create_account = "insert into accounts" + "(username,password)" + " values " + "(?, ?)";

    public void create(String name, String pass) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(create_account);
        preparedStatement.setString(1, name);
        preparedStatement.setString(2, pass);
        preparedStatement.executeUpdate();
    }

}
