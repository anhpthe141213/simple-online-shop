/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cart;
import model.Purchase;

/**
 *
 * @author msi
 */
public class cartDAO extends BaseDAO<Cart> {

    @Override
    public ArrayList<Cart> getAll() {
        ArrayList<Cart> ca = new ArrayList<>();
        try {
            String sql = "SELECT [id]\n"
                    + "      ,[productname]\n"
                    + "      ,[buyer]\n"
                    + "      ,[quantity]\n"
                    + "      ,[total]\n"
                    + "  FROM [cart]";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Cart a = new Cart();
                a.setId(rs.getInt("id"));
                a.setProName(rs.getString("productname"));
                a.setBuyer(rs.getString("buyer"));
                a.setTotalPrice(rs.getString("total"));
                a.setQuantity(rs.getInt("quantity"));
                ca.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ca;
    }

    String create_cart = "insert into cart" + "(productname,buyer,quantity,total)" + " values " + "(?, ?, ?, ?)";

    public void create(String productname, String total, int quantity, String buyer) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(create_cart);
        preparedStatement.setString(1, productname);
        preparedStatement.setString(2, buyer);
        preparedStatement.setInt(3, quantity);
        preparedStatement.setString(4, total);

        preparedStatement.executeUpdate();
    }
    String create_cart_history = "insert into history" + "(productname,buyer,quantity,total,buyDate)" + " values " + "(?, ?, ?, ?, ?)";

    public void create_ch(String productname, String total, int quantity, String buyer, String buyDate) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(create_cart_history);
        preparedStatement.setString(1, productname);
        preparedStatement.setString(2, buyer);
        preparedStatement.setInt(3, quantity);
        preparedStatement.setString(4, total);
        preparedStatement.setString(5, buyDate);

        preparedStatement.executeUpdate();
    }

    public void delete() throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM cart");
        preparedStatement.executeUpdate();
    }

    private String delete_item = "delete from cart where id = ?";

    public void deleteID(String id) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(delete_item);
        preparedStatement.setString(1, id);
        preparedStatement.executeUpdate();
    }

    String get_From_Name = "select * from history where buyer = ?";

    public ArrayList<Purchase> getHistory(String user) throws SQLException {
        ArrayList<Purchase> pur = new ArrayList<>();
        try {
            String sql = "SELECT\n"
                    + "       [productname]\n"
                    + "      ,[buyer]\n"
                    + "      ,[quantity]\n"
                    + "      ,[total]\n"
                    + "      ,[buyDate]\n"
                    + "  FROM [history] where [buyer] = ?";

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, user);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Purchase a = new Purchase();
                a.setBuyDate(rs.getString("buydate"));
                a.setProName(rs.getString("productname"));
                a.setBuyer(rs.getString("buyer"));
                a.setTotalPrice(rs.getString("total"));
                a.setQuantity(rs.getInt("quantity"));
                pur.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pur;
    }
}
