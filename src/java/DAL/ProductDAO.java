/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author msi
 */
public class ProductDAO extends BaseDAO<Product> {

    @Override
    public ArrayList<Product> getAll() {
        ArrayList<Product> products = new ArrayList<>();
        try {
            String sql = "SELECT [id]\n"
                    + "      ,[name]\n"
                    + "      ,[description]\n"
                    + "      ,[price]\n"
                    + "      ,[img]\n"
                    + "  FROM [product]";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                p.setId(rs.getInt("id"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));
                p.setPrice(rs.getString("price"));
                p.setImg(rs.getString("img"));
                products.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return products;
    }

    public Product selectById(String id) throws SQLException {
        Product p = new Product();
        try {

            String sql = "SELECT [id]\n"
                    + "      ,[name]\n"
                    + "      ,[description]\n"
                    + "      ,[price]\n"
                    + "      ,[img]\n"
                    + "  FROM [product]" + " where id = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {

                p.setId(rs.getInt("id"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));
                p.setPrice(rs.getString("price"));
                p.setImg(rs.getString("img"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return p;
    }

    String create_Product = "insert into product" + "(name,description,price,img)" + " values " + "( ?, ?, ?, ?)";

    public void createProduct(int id, String name, String des, String price, String img) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(create_Product);
//        preparedStatement.setInt(1, id);
        preparedStatement.setString(1, name);
        preparedStatement.setString(2, des);
        preparedStatement.setString(3, price);
        preparedStatement.setString(4, img);
        preparedStatement.executeUpdate();
    }
    String update_Product = "update product set name = ?, description = ?, price = ?,img = ? where id = ?";

    public void updateProduct(String id, String name, String des, String price, String img) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(update_Product);
        preparedStatement.setString(1, name);
        preparedStatement.setString(2, des);
        preparedStatement.setString(3, price);
        preparedStatement.setString(4, img);
        preparedStatement.setString(5, id);
        preparedStatement.executeUpdate();
    }
    String delete_Product = "delete from product where id  = ?";

    public void delete(String id) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(delete_Product);
        preparedStatement.setString(1, id);
        preparedStatement.executeUpdate();
    }
}
