package controller;

import DAL.cartDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "cartServlet", urlPatterns = {"/cartServlet"})
public class cartServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        cartDAO db = new cartDAO();
//        String id = request.getParameter("id");
//        response.getWriter().write(id);
//        try {
//            db.deleteID(id);
//        } catch (SQLException ex) {
//            Logger.getLogger(register.class.getName()).log(Level.SEVERE, null, ex);
//            response.getWriter().println("error");
//        }
        response.sendRedirect("cart.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("proName");
        String buyer = request.getParameter("buyer");
        String price = request.getParameter("price");
        int quantity = Integer.parseInt(request.getParameter("quantity"));

        String total = String.valueOf(Double.parseDouble(price) * quantity);

        cartDAO db = new cartDAO();
        try {
            db.create(name, total, quantity, buyer);
            response.sendRedirect("mainServlet");
        } catch (SQLException ex) {
            Logger.getLogger(register.class.getName()).log(Level.SEVERE, null, ex);
            response.getWriter().println("error");
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        cartDAO db = new cartDAO();
        String id = request.getParameter("id");
        response.getWriter().write(id);
        try {
            db.deleteID(id);
        } catch (SQLException ex) {
            Logger.getLogger(register.class.getName()).log(Level.SEVERE, null, ex);
            response.getWriter().println("error");
        }
        response.sendRedirect("cart.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
