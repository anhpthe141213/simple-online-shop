/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAL.ProductDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Product;

/**
 *
 * @author msi
 */
@WebServlet(name = "updateProServlet", urlPatterns = {"/updateProServlet"})
public class updateProServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDAO db = new ProductDAO();
        String id = request.getParameter("id");
        request.setAttribute("id", id);
        Product a = new Product();
        String name = null;
        String price = null;
        String des = null;
        String img = null;

        try {
            a = db.selectById(id);
            name = a.getName();
            price = a.getPrice();
            des = a.getDescription();
            img = a.getImg();
            request.setAttribute("name", name);
            request.setAttribute("price", price);
            request.setAttribute("des", des);
            request.setAttribute("img", img);
            request.getRequestDispatcher("updateProduct.jsp").forward(request, response);

        } catch (SQLException ex) {
            Logger.getLogger(productDetailServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id2");
        String name = request.getParameter("name");
        String des = request.getParameter("des");
        String price = request.getParameter("price");
        String img = request.getParameter("img");

        try {
            ProductDAO db = new ProductDAO();
            db.updateProduct(id, name, des, price, img);
//            response.getWriter().write(id);
            response.sendRedirect("adminServlet");
        } catch (SQLException ex) {
            Logger.getLogger(addProServlet.class.getName()).log(Level.SEVERE, null, ex);
            response.getWriter().write("Null cmnr");
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
