/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAL.accountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;

/**
 *
 * @author msi
 */
@WebServlet(name = "register", urlPatterns = {"/register"})
public class register extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet register</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet register at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        accountDAO db = new accountDAO();
        String username = request.getParameter("user");
        String password = request.getParameter("pass");
        String repassword = request.getParameter("repass");

        String error = "";

        HttpSession session = request.getSession();

        if (username.equals("")) {
            error = "Enter username";
            request.setAttribute("error", error);
            request.getRequestDispatcher("register.jsp").forward(request, response);
        }
        if (password.equals("")) {
            error = "Enter password";
            request.setAttribute("error", error);
            request.getRequestDispatcher("register.jsp").forward(request, response);
        }
        if (repassword.equals("")) {
            error = "Repeat password";
            request.setAttribute("error", error);
            request.getRequestDispatcher("register.jsp").forward(request, response);
        }
        if (!password.equals(repassword)) {
            error = "Repeat password is not the same as password";
            request.setAttribute("error", error);
            request.getRequestDispatcher("register.jsp").forward(request, response);
        }
        ArrayList<Account> acc = db.getAll();
        for (Account a : acc) {
            if (username.equals(a.getUser())) {
                error = "Username already exist!";
                request.setAttribute("error", error);
                request.getRequestDispatcher("register.jsp").forward(request, response);
            }
        }

        try {
            db.create(username, password);
            session.setAttribute("userName", username);
            response.sendRedirect("mainServlet");
        } catch (SQLException ex) {
            Logger.getLogger(register.class.getName()).log(Level.SEVERE, null, ex);
            response.getWriter().println("error");
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
