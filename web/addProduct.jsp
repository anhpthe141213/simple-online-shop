<%@page import="DAL.ProductDAO"%>
<%@page import="model.Product"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="styles.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>Moonlit Cafe</title>
        <%
            String userName = (String) request.getSession().getAttribute("userName");
        %>

    </head>
    <body>
        <div style="height: 5px;background-color: burlywood"></div>
        <%
            if (userName != null) {
        %>
        <ul>
            <li><a href="adminServlet"><i class="fa fa-fw fa-home"></i>Home</a></li>
            <li><a href="#news"><i class="fa fa-plus-square"></i>  Add product</a></li>


            <li style="float:right;"><a href="logout">Logout</a></li>
            <li style="float:right;" class="active"><a href="#"><i class="fa fa-address-card"></i>  <%=userName%></a></li>


        </ul>
        <%} else {%>
        <ul>
            <li><a href="mainServlet"><i class="fa fa-fw fa-home"></i>Home</a></li>
            <li><a href="#news"><i class="fa fa-file-text-o"></i> News</a></li>
            <li><a href="#contact"><i class="fa fa-commenting-o"></i> Contact</a></li>
            <li style="float:right;" class="active"><a href="login.jsp"></i> Login</a></li>
        </ul>
        <%}%>
        <hr>
        <div  class="logo"><img src="Image/Logo.png" alt="Logo" class="logogo"></div>
        <hr>
        <div class="container2" style="background-color: burlywood;padding: 10px">
            <center>
                <form action="addProServlet" method="post">
                    <table>
                        <tr>
                            <td>Product name: </td>
                            <td><input type="text" name="name" class="adduppro"><br></td>
                        </tr>
                        <tr>
                            <td>Description: </td>
                            <td><textarea name="des" rows="5" cols="100" class="adduppro">${requestScope.des}</textarea><br></td>
                        </tr>
                        <tr>
                            <td>Price: </td>
                            <td><input type="text" name="price" class="adduppro"><br></td>
                        </tr>
                        <tr>
                            <td>Image URL: </td>
                            <td><input type="text" name="img" class="adduppro"><br></td>
                        </tr>
                    </table>
                    <input type="submit" value="Add new product" class="abtn">
                </form>
            </center>
        </div>
        <hr>



        <footer id="footer" class="section footer">
            <div class="container">
                <div class="footer__top">
                    <div class="footer-top__box">
                        <h3>EXTRAS</h3>
                        <a href="#">Brands</a>
                        <a href="#">Gift Certificates</a>
                        <a href="#">Affiliate</a>
                        <a href="#">Specials</a>
                        <a href="#">Site Map</a>
                    </div>
                    <div class="footer-top__box">
                        <h3>INFORMATION</h3>
                        <a href="#">About Us</a>
                        <a href="#">Privacy Policy</a>
                        <a href="#">Terms & Conditions</a>
                        <a href="#">Contact Us</a>
                        <a href="#">Site Map</a>
                    </div>
                    <div class="footer-top__box">
                        <h3>MY ACCOUNT</h3>
                        <a href="#">My Account</a>
                        <a href="#">Order History</a>
                        <a href="#">Wish List</a>
                        <a href="#">Newsletter</a>
                        <a href="#">Returns</a>
                    </div>
                    <div class="footer-top__box">
                        <h3>CONTACT US</h3>
                        <div>
                            42 Dream House, Dreammy street, 7131 Dreamville, USA
                        </div>
                        <div>
                            company@gmail.com
                        </div>
                        <div>
                            456-456-4512
                        </div>
                        <div>
                            Dream City, USA
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer__bottom">
                <div class="footer-bottom__box">

                </div>
                <div class="footer-bottom__box">

                </div>
            </div>

        </footer>
    </body>
</html>
