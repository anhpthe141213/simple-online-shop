<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="Loginstyle.css">
    </head>
    <body>
        <form action="register" method="get">
            <div class="login-box">
                <h1>Login</h1>
                <div class="textbox">
                    <i class="fas fa-user"></i>
                    <input type="text" placeholder="Username" name="user">
                </div>

                <div class="textbox">
                    <i class="fas fa-lock"></i>
                    <input type="password" placeholder="Password" name="pass">
                </div>

                <div class="textbox">
                    <input type="password" placeholder="Repeat password" name="repass">
                </div>
                <div><p>${requestScope.error}</p></div>
                <input type="submit" class="btn" value="Register">
                <a href="mainServlet" class="return">Back to front page</a>
            </div>
        </form>
    </body>
</html>
