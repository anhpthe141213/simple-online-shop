<%@page import="DAL.ProductDAO"%>
<%@page import="model.Product"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="styles.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>Moonlit Cafe</title>
        <%
            String userName = (String) request.getSession().getAttribute("userName");
        %>

    </head>
    <body>
        <div style="height: 5px;background-color: burlywood"></div>
        <%
            if (userName != null) {
        %>
        <ul>
            <li><a href="adminServlet"><i class="fa fa-fw fa-home"></i>Home</a></li>
            <li><a href="addProduct.jsp"><i class="fa fa-plus-square"></i>  Add product</a></li>


            <li style="float:right;"><a href="logout">Logout</a></li>
            <li style="float:right;" class="active"><a href="#"><i class="fa fa-address-card"></i>  <%=userName%></a></li>


        </ul>
        <%} else {%>
        <ul>
            <li><a href="mainServlet"><i class="fa fa-fw fa-home"></i>Home</a></li>
            <li><a href="#news"><i class="fa fa-file-text-o"></i> News</a></li>
            <li><a href="#contact"><i class="fa fa-commenting-o"></i> Contact</a></li>
            <li style="float:right;" class="active"><a href="login.jsp"></i> Login</a></li>
        </ul>
        <%}%>
        <hr>
        <div  class="logo"><img src="Image/Logo.png" alt="Logo" class="logogo"></div>
        <hr>
        <div class="container2">
            <center><table>
                    <tr>
                        <td>
                    <tr>
                        <td><div class="slideshow-container">

                                <div class="mySlides fade">
                                    <div class="numbertext">1 / 3</div>
                                    <img src="Image/Slide1.jpg" class="slide_img">
                                    <div class="text">Welcome to</div>
                                </div>

                                <div class="mySlides fade">
                                    <div class="numbertext">2 / 3</div>
                                    <img src="Image/Slide2.jpg" class="slide_img">
                                    <div class="text">Moonlit Cafe</div>
                                </div>

                                <div class="mySlides fade">
                                    <div class="numbertext">3 / 3</div>
                                    <img src="Image/Slide3.jpg" class="slide_img">
                                    <div class="text">Enjoy your stay</div>
                                </div>

                            </div>
                            <br>

                            <div style="text-align:center">
                                <span class="dot"></span> 
                                <span class="dot"></span> 
                                <span class="dot"></span> 
                            </div>
                            <script type="text/javascript" src="script.js"></script>
                        </td>
                        </center>
                    </tr>
                </table></center>
        </div>
        <hr>
        <div style="text-align: center;background-color: burlywood"><h2>Available Products</h2></div>
        <div class="container2">
            <table style="padding: 10px">
                <%
                    ArrayList<Product> product = (ArrayList<Product>) request.getAttribute("pro");
                %>
                <% for (Product p : product) {
                %>
                <tr>
                    <td style="border: solid"><img src="<%=p.getImg()%>" style="height: 250px;width: 250px"></td>
                    <td style="width: 100%;border: solid;border-color: burlywood ">
                <center>

                    <p><%=p.getName()%></p>
                    <p><%=p.getDescription()%></p>
                    <p><%=p.getPrice()%></p>
                    <p></p>
                    <a href="deleteProServlet?id=<%=p.getId()%>" class="abtn">Delete</a>
                    <a href="updateProServlet?id=<%=p.getId()%>" class="abtn">Update</a>
                </center>

                </td>
                </tr>
                <%}%>

            </table>

        </div>
        <hr>



        <footer id="footer" class="section footer">
            <div class="container">
                <div class="footer__top">
                    <div class="footer-top__box">
                        <h3>EXTRAS</h3>
                        <a href="#">Brands</a>
                        <a href="#">Gift Certificates</a>
                        <a href="#">Affiliate</a>
                        <a href="#">Specials</a>
                        <a href="#">Site Map</a>
                    </div>
                    <div class="footer-top__box">
                        <h3>INFORMATION</h3>
                        <a href="#">About Us</a>
                        <a href="#">Privacy Policy</a>
                        <a href="#">Terms & Conditions</a>
                        <a href="#">Contact Us</a>
                        <a href="#">Site Map</a>
                    </div>
                    <div class="footer-top__box">
                        <h3>MY ACCOUNT</h3>
                        <a href="#">My Account</a>
                        <a href="#">Order History</a>
                        <a href="#">Wish List</a>
                        <a href="#">Newsletter</a>
                        <a href="#">Returns</a>
                    </div>
                    <div class="footer-top__box">
                        <h3>CONTACT US</h3>
                        <div>
                            42 Dream House, Dreammy street, 7131 Dreamville, USA
                        </div>
                        <div>
                            company@gmail.com
                        </div>
                        <div>
                            456-456-4512
                        </div>
                        <div>
                            Dream City, USA
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer__bottom">
                <div class="footer-bottom__box">

                </div>
                <div class="footer-bottom__box">

                </div>
            </div>

        </footer>
    </body>
</html>
