<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="Loginstyle.css">
    </head>
    <body>
        <form action="login" method="get">
            <div class="login-box">
                <h1>Login</h1>
                <div class="textbox">
                    <i class="fas fa-user"></i>
                    <input type="text" placeholder="Username" name="username">
                </div>

                <div class="textbox">
                    <i class="fas fa-lock"></i>
                    <input type="password" placeholder="Password" name="password">
                </div>
                <input type="checkbox" name="remember"/>Remember me<br>
                <div><p>${requestScope.wrong}</p></div>
                <input type="submit" class="btn" value="Sign in">
                <center>
                    <p>Don't have an account</p>
                    <a href="register.jsp" class="return">Register</a><br><hr>
                    <a href="mainServlet" class="return">Back to front page</a>
                </center>

            </div>
        </form>
    </body>
</html>
