<%@page import="DAL.cartDAO"%>
<%@page import="model.Cart"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="styles.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>Moonlit Cafe</title>
        <%
            String userName = (String) request.getSession().getAttribute("userName");
        %>

    </head>
    <body>
        <div style="height: 5px;background-color: burlywood"></div>
        <%
            if (userName != null) {
        %>
        <ul>
            <li><a href="mainServlet"><i class="fa fa-fw fa-home"></i>Home</a></li>
            <li><a href="#news"><i class="fa fa-file-text-o"></i> News</a></li>
            <li><a href="#contact"><i class="fa fa-commenting-o"></i> Contact</a></li>


            <li style="float:right;"><a href="logout">Logout</a></li>
            <li style="float:right;" class="active"><a href="cart.jsp"><i class="fa fa-shopping-cart"></i> Cart</a></li>
            <li style="float: right" class="active"><a href="purchaseHistory.jsp"  ><i class="fa fa-bar-chart"></i> Purchase history</a></li>  
            <li style="float:right;" class="active"><a href="#"><i class="fa fa-address-card"></i>  <%=userName%></a></li>


        </ul>
        <%} else {%>
        <ul>
            <li><a href="mainServlet"><i class="fa fa-fw fa-home"></i>Home</a></li>
            <li><a href="#news"><i class="fa fa-file-text-o"></i> News</a></li>
            <li><a href="#contact"><i class="fa fa-commenting-o"></i> Contact</a></li>
            <li style="float:right;" class="active"><a href="login.jsp"></i> Login</a></li>
        </ul>
        <%}%>
        <hr>
        <div  class="logo"><img src="Image/Logo.png" alt="Logo" class="logogo"></div>
        <hr>
        <div class="grid-container">
            <%
                cartDAO db = new cartDAO();
                ArrayList<Cart> cart = db.getAll();
                if (cart != null) {
            %>
            <div class="item3">
                <div class="container2">
                    <center>
                        <table border="1" style="width: 100%;color: white">
                            <tr>
                                <td style="background-color: #f2f2f2;color: black;font-size: 20">
                                    ID
                                </td>
                                <td style="background-color: #717171;color: white;font-size: 20">
                                    Product name
                                </td >
                                <td style="background-color: #f2f2f2;color: black;font-size: 20">
                                    Buyer
                                </td>
                                <td style="background-color: #717171;color: white;font-size: 20">
                                    Quantity
                                </td>
                                <td style="background-color: #f2f2f2;color: black;font-size: 20">
                                    Total
                                </td>
                            </tr>
                            <tr style="border: none;height: 3px;background-color: saddlebrown"></tr>
                            <% for (Cart s : cart) {
                            %>
                            <tr>
                                <td style="background-color: burlywood;color: black">
                                    <%=s.getId()%>
                                </td> 
                                <td style="background-color: chocolate">
                                    <%=s.getProName()%>
                                </td>
                                <td style="background-color: burlywood;color: black">
                                    <%=s.getBuyer()%>
                                </td>
                                <td style="background-color: chocolate">
                                    <%=s.getQuantity()%>
                                </td>
                                <td style="background-color: burlywood;color: black">
                                    <%=s.getTotalPrice()%> VND
                                </td>
                                <td style="width: 25px;">
                            <center>
                                <form action="cartServlet" method="post">
                                    <input type="hidden" name="id" value="<%=s.getId()%>">
                                    <input type="submit" value="X">
                                </form>                         
                            </center>
                            </td>
                            </tr>
                            <%}%>
                        </table>
                    </center>
                </div>
            </div>

            <%} else {%>
            <center><h1>Empty</h1></center>
                <%}%>
            <hr>
            <center>
                <a href="historyServlet" class="abtn">Buy all</a>
            </center>
            <hr>

        </div>
    </div>
</body>
</html>
