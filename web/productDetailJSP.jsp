<%-- 
    Document   : productDetailJSP
    Created on : Nov 8, 2020, 8:47:17 PM
    Author     : msi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Detail: ${requestScope.name}</title>
        <link rel="stylesheet" href="pdStyles.css">
        <link rel="stylesheet" href="styles.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>

    <%
        String userName = (String) request.getSession().getAttribute("userName");
    %>
    <body>

        <div class="grid-container">
            <div class="item1">

                <%
                    if (userName != null) {
                %>
                <ul>
                    <li><a href="mainServlet"><i class="fa fa-fw fa-home"></i>Home</a></li>
                    <li><a href="#news"><i class="fa fa-file-text-o"></i> News</a></li>
                    <li><a href="#contact"><i class="fa fa-commenting-o"></i> Contact</a></li>

                    <li style="float:right;"><a href="logout">Logout</a></li>
                    <li style="float:right;" class="active"><a href="cart.jsp"><i class="fa fa-shopping-cart"></i> Cart</a></li>
                    <li style="float: right" class="active"><a href="purchaseHistory.jsp"  ><i class="fa fa-bar-chart"></i> Purchase history</a></li>    

                    <li style="float:right;" class="active"><a href="#"><i class="fa fa-address-card"></i>  <%=userName%></a></li>


                </ul>
                <%} else {%>
                <ul>
                    <li><a href="mainServlet"><i class="fa fa-fw fa-home"></i>Home</a></li>
                    <li><a href="#news"><i class="fa fa-file-text-o"></i> News</a></li>
                    <li><a href="#contact"><i class="fa fa-commenting-o"></i> Contact</a></li>
                    <li style="float:right;" class="active"><a href="login.jsp"></i> Login</a></li>
                </ul>
                <%}%>


            </div>
            <div class="item4"><div  class="logo"><img src="Image/Logo.png" alt="Logo" class="logogo"></div></div>
            <div class="item2">
                <div class="card">
                    <div class="images">
                        <h2  style="background-color: chocolate">${requestScope.name}</h2>
                        <div class="slider"><img id="big-image" src="${requestScope.img}" alt=""></div>

                    </div>
                    <div class="infos">
                        <h1>${requestScope.name}</h1>

                        <div class="price" style="background-color: burlywood">
                            <h3 >${requestScope.price} VND</h3>
                        </div>
                        <div id="more-infos"  style="background-color: burlywood">
                            <h5 class="choose">Description</h5>
                        </div>
                        <div id="info-content">
                            <p  class="paragraph" style="display: block;">${requestScope.des}</p>

                        </div>
                        <form action="cartServlet" method="get">
                            <div class="quantity"  style="background-color: burlywood">
                                <h3>Quantity:</h3>
                                <input type="number" name="quantity" id="counter" min="1" value="1">
                            </div>
                            <div class="buttons">

                                <%
                                    String proname = (String) request.getAttribute("name");
                                    String price = (String) request.getAttribute("price");

                                %>
                                <input type="hidden" value="<%=proname%>" name ="proName">
                                <input type="hidden" value="<%=price%>" name ="price">
                                <input type="hidden" value="<%=userName%>" name ="buyer">
                                <input type="submit" id="add-to-cart" value="Add to cart"></button

                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="item3">
                <footer id="footer" class="section footer">
                    <div class="container">
                        <div class="footer__top">
                            <div class="footer-top__box">
                                <h3>EXTRAS</h3>
                                <a href="#">Brands</a>
                                <a href="#">Gift Certificates</a>
                                <a href="#">Affiliate</a>
                                <a href="#">Specials</a>
                                <a href="#">Site Map</a>
                            </div>
                            <div class="footer-top__box">
                                <h3>INFORMATION</h3>
                                <a href="#">About Us</a>
                                <a href="#">Privacy Policy</a>
                                <a href="#">Terms & Conditions</a>
                                <a href="#">Contact Us</a>
                                <a href="#">Site Map</a>
                            </div>
                            <div class="footer-top__box">
                                <h3>MY ACCOUNT</h3>
                                <a href="#">My Account</a>
                                <a href="#">Order History</a>
                                <a href="#">Wish List</a>
                                <a href="#">Newsletter</a>
                                <a href="#">Returns</a>
                            </div>
                            <div class="footer-top__box">
                                <h3>CONTACT US</h3>
                                <div>
                                    42 Dream House, Dreammy street, 7131 Dreamville, USA
                                </div>
                                <div>
                                    company@gmail.com
                                </div>
                                <div>
                                    456-456-4512
                                </div>
                                <div>
                                    Dream City, USA
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer__bottom">
                        <div class="footer-bottom__box">

                        </div>
                        <div class="footer-bottom__box">

                        </div>
                    </div>

                </footer>
            </div>
        </div>
        <script src="app.js"></script>



    </body>
</html>
