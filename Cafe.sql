﻿create database cafe
use cafe
create table product(
id int not null identity(1,1),
name nvarchar(32) not null,
description nvarchar(max)not null,
price nvarchar(15) not null,
img nvarchar(100) not null,
primary key (id)
)
go
set identity_insert product on
go
insert into product(id,name,description,price,img) values (1,'Black Coffee','Black coffee is simply coffee that is normally brewed without the addition of additives such as sugar, milk, cream or added flavours. While it has a slightly bitter taste compared to when it is flavoured with additives, many people love a strong cup of black coffee. In fact, for some, it is part of their everyday diet.','30000','Image/Product/black_coffee.jpg')
insert into product(id,name,description,price,img) values (2,'Cappuccino','Cappuccino is a coffee drink that today is typically composed of a single espresso shot and hot milk, with the surface topped with foamed milk. Cappuccinos are most often prepared with an espresso machine.','50000','Image/Product/cappuccino.jpg')
insert into product(id,name,description,price,img) values (3,'Latte','Caffe latte (or simply latte) is a coffee drink made with espresso and steamed milk. The word comes from the Italian caffe e latte, caffelatte or caffellatte , which means "coffee & milk". The word is also sometimes spelled latte or latte in English with different kinds of accent marks, which can be a hyperforeignism or a deliberate attempt to indicate that the word is not pronounced according to the rules of English orthography.','45000','Image/Product/latte.jpg')
insert into product(id,name,description,price,img) values (4,'Caramel Latte','A Caramel Latte is made by mixing espresso with caramel syrup and pouring steamed milk on top. A Caramel Macchiato is made by mixing vanilla syrup and steamed milk together, and then pouring the espresso on top, leaving the black mark on the milk foam.','50000','Image/Product/caramel-latte.jpg')
insert into product(id,name,description,price,img) values (5,'Mocha','A caffe mocha also called mocaccino is a chocolate-flavoured variant of a caffe latte Other commonly used spellings are mochaccino and also mochachino.','35000','Image/Product/caramel-mocha.jpg')
insert into product(id,name,description,price,img) values (6,'Coffee Milk Tea','Coffee flavor Milk Tea','25000','Image/Product/coffee_milktea.jpg')
insert into product(id,name,description,price,img) values (7,'Iced Coffee','Black coffee with ices','30000','Image/Product/iced-coffee.jpg')
insert into product(id,name,description,price,img) values (8,'Whipped Coffee','Essentially, dalgona coffee is whipped coffee. It is made with 4 simple ingredients – instant coffee, sugar, hot water and milk. Coffee, sugar and hot water is added into a bowl and whisked until it reaches creamy, velvety and stiff peak status.','40000','Image/Product/whipped-coffee.jpg')
select * from product

create table accounts(
username nvarchar(32) not null,
password nvarchar(32)not null,
primary key (username)
)
create table cart(
id int not null identity(1,1),
productname nvarchar(32) not null,
buyer nvarchar(32) not null,
quantity int not null,
total nvarchar(30)not null,
primary key (id)
)

create table history(
productname nvarchar(32) not null,
buyer nvarchar(32) not null,
quantity int not null,
total nvarchar(30)not null,
buydate nvarchar(50)not null
)

